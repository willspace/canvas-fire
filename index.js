const canvas = document.createElement('canvas')
document.body.appendChild(canvas)
canvas.width = document.documentElement.clientWidth
canvas.height = window.innerHeight
console.log(window.innerWidth, window.innerHeight)
const ctx = canvas.getContext('2d')

ctx.translate(0, canvas.height)
ctx.scale(1, -1)

const fireworkArray = []
const bomArray = []
// 烟花
class Fireworks {
  constructor(x, y) {
    this.x = x
    this.y = y
    this.r = 6
    this.opacity = 1
    this.count = 500
    this.particles = [] // 所有的粒子
  }
  draw() {
    this.opacity = this.opacity > 0.2 ? this.opacity : 0.2
    for (let i = 0; i < 100; i++) {
      const ball = new Ball(
        this.x,
        this.y - i,
        this.r - i / 20,
        `rgba(200,200,50,${this.opacity - i / 100})`
      )
      ball.draw()
    }
  }
  bom() {
    if (this.particles.length === 0) {
      const hd = (Math.PI * 2) / this.count
      for (let i = 0; i < this.count; i++) {
        const color = `rgba(${Math.random() * 256},${Math.random() * 256},${Math.random() * 256},1)`
        const dirx = Math.cos(hd * i) * Math.random() * 4
        const diry = Math.sin(hd * i) * Math.random() * 4
        const particle = new Particle(this.x, this.y, dirx, diry, color, i % 2 == 0)
        this.particles.push(particle)
        particle.draw()
      }
    } else {
      this.particles.forEach(particle => {
        particle.update()
      })
    }
  }
}

class Ball {
  constructor(x, y, r, color) {
    this.x = x
    this.y = y
    this.r = r
    this.color = color
  }
  draw() {
    ctx.save()
    ctx.beginPath()
    ctx.fillStyle = this.color
    ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2)
    ctx.fill()
    ctx.restore()
  }
}

class Particle {
  constructor(x, y, dirx, diry, color, type) {
    this.x = x
    this.y = y
    this.r = 2
    this.type = type
    this.dirx = dirx
    this.diry = diry
    this.color = color
  }
  draw() {
    if (this.type) {
      ctx.save()
      ctx.beginPath()
      ctx.fillStyle = this.color
      ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2)
      ctx.fill()
      ctx.restore()
    }
    this.type = !this.type
  }
  update() {
    this.x += this.dirx
    this.y += this.diry
    this.dirx *= 0.99
    this.diry *= 0.98
    this.draw()
  }
}

let sum = 0

function move() {
  ctx.clearRect(0, 0, canvas.width, canvas.height)
  if (sum % 50 === 0) {
    // 释放一个新的烟花
    const x = (Math.random() * canvas.width * 3) / 4 + canvas.width / 8
    const y = Math.random() * 100
    const fire = new Fireworks(x, y)
    fireworkArray.push(fire)
  }
  if (fireworkArray.length === 4) {
    const arr = fireworkArray.shift()
    bomArray.push(arr)
  }

  if (bomArray.length === 4) {
    bomArray.shift()
  }
  fireworkArray.forEach((fire, i) => {
    fire.draw()
    fire.y += 5
    fire.opacity -= 0.01
  })

  bomArray.forEach(fire => {
    fire.bom()
  })

  sum++

  requestAnimationFrame(move)
}
move()
